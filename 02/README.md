# [Advent of Code: Day 2](http://adventofcode.com/2017/day/2)

## [Kona](https://github.com/kevinlawler/kona)

[Kona](https://github.com/kevinlawler/kona) is a *very* esoteric language. But I can see the appeal; coding in Kona is like a puzzle on its own. Of course, this means *reading* Kona is nearly impossible.

I struggled a lot to figure out how to parse Advent of Code's tsv format into a Kona matrix, and eventually just gave up on the idea. This means you have to hard-code your matrix into the program itself, which I don't love, but as a first-time Kona user, I can live with it.

## Usage

First, convert your Advent of Code puzzle to a Kona matrix:

	cat | tr "\t" ',' | tr "\n" ';' | rev | cut -c 2- | rev | xargs printf "(%s)"

(Type or paste your puzzle input, then press `^D`. Copy the result.)

Replace the fourth line of `main.k` with the result. This line begins with `puzzle:`

Then run it!

	./k main.k

## Tests

From the Advent of Code page:

```text
puzzle:(5 1 9 5;7 5 3;2 4 6 8) / => 16
```

## Code Walkthrough

Yes, this is effectively a one-liner. So let's go through it. The line in question is line 7, or:

```text
+/{-/(|/x; &/x)}'puzzle
```

Confusingly, to read Kona, we have to read backwards sometimes. So let's start at the end of this line.

`'puzzle` tells Kona to perform an operation on every element of our symbol, `puzzle`. (You can think of "symbol" as Kona's word for "variable.") You can think of `'` as Kona's word for `foreach`.

Since `puzzle` is a matrix, its elements are vectors. So we're going to perform an operation on every vector (row) of `puzzle`.

The operation we're going to perform is

```text
{-/(|/x;&/x)}
```

This operation is a function, since it is between braces `{` and `}`. This function returns the difference between the highest and lowest value in a vector. Note that the first (and in our case, only) argument passed to the function is represented by `x`.

Once again reading backwards, the first bit we have to look at is `(|/x;&/x)`. The parentheses indicate that this is either a vector or a matrix. In this case, it's a vector with two elements. The elements are separated by `;`.

The first element of our vector is `|/x`. `/x` tells the next operation to consider the *elements* of `x` rather than `x` itself.

So, for instance, consider a vector `(1 2 3)`. The expression `1+(1 2 3)` will see the vector, create a new vector, `1 1 1`, and add the two vectors together, giving us `2 3 4`. In contrast, the expression `1+/(1 2 3)` will apply the `+` operation to every element of the vector, or `1 + 1 + 2 + 3`, giving us `7`.

Back to our Advent of Code puzzle, the `|` operation returns the maximum value it receives. `/` makes sure we give it every value in our row, so put together `|/` gives us the maximum value in a vector.

Similarly, `&` returns the minimum value it receives. `&/` gives us the minimum value in a vector.

So after taking a vector as input, we produce a new vector with two elements, `(max; min)`.

Still within our function, all that's left is `-/`. Once again we use `/` to consider all elements of the vector rather than the vector as a whole. Then we use `-` to subtract them. In other words, we pull apart the vector `(max; min)` using the subtraction operator, `-`. This gives us `max - min`.

So that's our function. Given a matrix, it calculates the difference between the maximum and minimum values in every row. This returns a new vector with those calculations. If we pass the puzzle example input matrix `(5 1 9 5;7 5 3;2 4 6 8)`, we get a vector back: `(8 4 6)`.

We compute the sum of this vector in much the same way we subtracted above. First we use `/` and then the `+` operation. This gives us the expression `8 + 4 + 6` which evaluates to `18`. The result automatically prints to the console, and we're done!
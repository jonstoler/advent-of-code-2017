# [Advent of Code: Day 5](http://adventofcode.com/2017/day/5)

## [D](https://dlang.org/)

D worked pretty well! I think it strikes a good middle ground between a low-level language and a higher-level language. I don't think I would normally use it over something like Go, but it was fun to try out regardless.

In particular, I think it's cool that you're encouraged to import dependencies within blocks rather than all at the beginning of your files. While this is hardly a feature unique to D, I don't think I've seen a programming language present this kind of importing as convention before.

## Usage

	dmd main.d
	./main [puzzle input]


## Tests

	./main "$(echo "0\n3\n0\n1\n-3")" # => 5

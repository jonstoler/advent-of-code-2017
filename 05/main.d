void main(string[] args) {
	import std.stdio: stderr, writeln, writefln;
	if(args.length < 2){
		stderr.writeln("Please provide puzzle input.");
		import std.c.stdlib: exit;
		exit(1);
	}

	string puzzleInput = args[1];

	import std.string: splitLines;
	auto lines = splitLines(puzzleInput);

	import std.conv;
	int[] offsets;
	foreach(string s; lines) {
		offsets ~= parse!int(s);
	}

	int index = 0;
	int steps = 0;
	while(index < offsets.length && index >= 0) {
		int jump = offsets[index];
		offsets[index]++;
		index += jump;
		steps++;
	}

	writefln("%d", steps);
}

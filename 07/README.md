# [Advent of Code: Day 7](http://adventofcode.com/2017/day/7)

## [Lua](https://www.lua.org/)

Lua is one of my favorite languages, and one I have a lot of experience with. Lua's pattern matching makes it a good candidate for string parsing, which this puzzle requires.

Since I didn't have to learn a new language this time around, I decided to comment my code more heavily than usual. Hopefully this makes it more readable, and it gave me something to try to make this challenge a little bit different than usual!

## Usage

	./main.lua [puzzle input]


## Tests

	./main.lua "$(cat # paste puzzle input)" # -> tknk

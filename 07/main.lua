#!/usr/bin/env lua

if #arg == 0 then
	io.stderr:write("Please provide puzzle input.\n")
	os.exit(1)
end

-- read puzzle input from stdin
local input = arg[1]

-- table to hold the names of all of our programs
-- more specifically, this table will store whether another
-- program holds each program
--
-- we've turned this puzzle around a little bit
-- our goal here is: find the program that is not being held by any others
-- this does not require us to reconstruct a tree!
local programs = {}

-- iterate through the lines of our input
for line in input:gmatch("[^\r\n]+") do
	-- extract the program name
	local name = line:gsub("^(.-) .*$", "%1")

	-- if it's not already set, initialize it (to NOT held by anything)
	programs[name] = programs[name] or false

	-- if this line specifies programs that are held...
	if line:match(" -> ") then
		-- iterate through those
		local holding = line:gsub("^(.-) -> (.-)$", "%2")
		for program in (holding .. ", "):gmatch("(.-), ") do
			-- set held status to true
			programs[program] = true
		end
	end
end

-- now find the sole un-held program, and print it out!
for k, v in pairs(programs) do
	if v == false then
		print(k)
		break
	end
end

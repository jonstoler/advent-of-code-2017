# [Advent of Code: Day 4](http://adventofcode.com/2017/day/4)

## [Ruby](https://www.ruby-lang.org/)

Surprisingly, this is my first time using Ruby. I've used extremely Ruby-like [Crystal](https://crystal-lang.org), so I didn't have any trouble.

Ruby's batteries-included approach made this solution short and simple. So I guess this was a success in Ruby's book!

## Usage

	./main.rb [puzzle input]


## Tests

	./main.rb "aa bb cc dd ee" # => 1
	./main.rb "aa bb cc dd aa" # => 0
	./main.rb "aa bb cc dd aaa" # => 1
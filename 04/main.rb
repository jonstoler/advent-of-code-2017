#!/usr/bin/env ruby

if ARGV.size < 1 then
	puts "Please specify puzzle input."
	exit 1
end

destination = ARGV[0]

lines = ARGV[0].split("\n")
valid_passphrases = 0
lines.each do |line|
	words = line.split(" ")
	valid_passphrases = valid_passphrases + 1 unless words != words.uniq
end

puts valid_passphrases
# [Advent of Code: 2017](http://adventofcode.com/2017/)

I'm doing Advent of Code this year, but with a slight twist.

I'm going to use a different programming language for each challenge. Some languages will be obscure. Some will be languages I already know; most probably won't.

After completing each challenge, I'll do a small write-up about my experience using that language for that challenge.

## Why?

Because these challenges are a good way to tinker with new programming languages and concepts. They're simple enough to be solvable but complex enough to require a little bit of research and planning.

I know that this isn't going to make me an expert it any particular language (or necessarily even remotely competent), but it will expose me to new tools, frameworks, and concepts. That's worth something!

## Challenges

| Day | Language | Completed |
| :-: | :------- | :-------- |
| [Dec 1](./01) | [GNU Guile](https://www.gnu.org/software/guile/learn/#tutorials) | Yes |
| [Dec 2](./02) | [Kona](https://github.com/kevinlawler/kona) | Yes |
| Dec 3 | | No |
| [Dec 4](./04) | [Ruby](https://www.ruby-lang.org/) | Yes |
| [Dec 5](./05) | [D](https://dlang.org/) | Yes |
| Dec 6 | | No |
| [Dec 7](./07) | [Lua](https://www.lua.org/) | Yes |

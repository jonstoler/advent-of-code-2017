; make sure the user has provided puzzle input
(if (< (length (command-line)) 2)
	(begin
		(display "Please include puzzle input as a command-line argument.")
		(exit)
	)
)

; get puzzle input from cli argument
(define puzzle-input (list-ref (command-line) 1))

; initialize our sum
(define matched-sum 0)

; initialize our list of provided digits
(define all-digits '())
; convert puzzle input into a list (easier to deal with)
(set! all-digits (string->list puzzle-input))

; iterate over items in list
(for-each (lambda (i)
	; define some local variables
	(let (
		; the digit at current index
		(current-digit (list-ref all-digits i))

		; the next digit (wraps around to the front)
		(next-digit (list-ref all-digits (modulo (+ i 1) (length all-digits))))
	)
		; compare the digits
		(if (char=? current-digit next-digit)
			; if they match, add the current digit to our sum
			; (convert to number first)

			; since there's no direct way to convert from a char to the number it represents
			; (you typically get char codes instead), we have to do char->string->number
			(set! matched-sum (+ (string->number (string current-digit)) matched-sum))
		)
	)
) (iota (length all-digits)))

; print out our sum!
(display matched-sum)
(newline)
# [Advent of Code: Day 1](http://adventofcode.com/2017/day/1)

## [GNU Guile](https://www.gnu.org/software/guile/learn/#tutorials)

I decided to try out a lisp today! Since I haven't used lisp before, I thought the first advent would be a good time to try it, before the puzzles get too difficult. I was right - I struggled a lot with lisp syntax.

First I tried using [SBCL](http://sbcl.org/), which I found to be too strict and poorly-documented.

I used ["Learn Scheme in 15 minutes"](http://web-artanis.com/scheme.html) as a primary reference, but I also dug into [the official Guile Reference Manual](https://www.gnu.org/software/guile/manual/html_node/index.html) for a few things.

## Usage

	guile -s main.scm

## Tests

From the Advent of Code page:

	guile -s main.scm 1122 # => 3
	guile -s main.scm 1111 # => 4
	guile -s main.scm 1234 # => 0
	guile -s main.scm 91212129 # => 9

## Thoughts/Notes

I must say, lisp was hard to grasp! Guile's lack of `for` loops, the way variables must be defined and set separately, and the prefix notation for operators and "methods" were small points of struggle. Small but big enough to be a bit challenging to overcome.

I didn't get to use any macros or metaprogramming for this simple example (macros would probably be too hard for a beginner like me to grok anyway), but now I understand better how lisp works. So overall I'm happy with the results.